from django.db import models

# Create your models here.

class Person(models.Model):
	name = models.CharField(max_length=30)
	sex = models.IntegerField()
	id = models.CharField(max_length=30, primary_key=True)
	birthday = models.DateField()
	